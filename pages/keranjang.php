<!-- Section-->
<section class="py-5">
    <div class="container">
        <div class="row justify-content-center">

            <?php

                $queryInvoice   = "SELECT kode_invoice FROM keranjang WHERE id_akun='$_SESSION[id_akun]' AND status='Keranjang' ";
                $prosesInvoice  = mysqli_query($koneksinya, $queryInvoice);
                $cekInvoice     = mysqli_num_rows($prosesInvoice);
                $resultInvoice  = mysqli_fetch_assoc($prosesInvoice);

                if ($cekInvoice<=0) {
                    $kode_invoiceNya    = NULL;
                }else{
                    $kode_invoiceNya    = $resultInvoice['kode_invoice'];
                }

            ?>

            <div class="col-10">
                <h2>INVOICE: <u><?= $kode_invoiceNya; ?></u></h2>
                <table class="table table-dark table-striped mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Sub Total</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            $no=1;
                            $totalBayar=0;
                            $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE id_akun='$_SESSION[id_akun]' AND keranjang.status='Keranjang' ORDER BY id_keranjang DESC";
                            $prosesBarang   = mysqli_query($koneksinya, $queryBarang);
                            $cekBarang      = mysqli_num_rows($prosesBarang);
                            if ($cekBarang<=0) {
                        ?>

                        <tr class="text-center">
                            <td scope="row" colspan="6"><i>Tidak ada data ...</i></td>
                        </tr>

                        <?php
                            }else{
                                while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                    $totalBayar+=$resultBarang['sub_jumlah'];

                        ?>
                        
                        <tr>
                            <th scope="row"><?= $no++; ?></th>
                            <td><?= $resultBarang['nama_barang']; ?></td>
                            <td>Rp<?= rp($resultBarang['harga']); ?></td>
                            <td><?= $resultBarang['qty']; ?></td>
                            <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                            <td>
                                <a href="pages/proses-hapus-keranjang.php?id_keranjang=<?= $resultBarang['id_keranjang']; ?>&id_barang=<?= $resultBarang['id_barang']; ?>&qty=<?= $resultBarang['qty']; ?>&stock=<?= $resultBarang['stock']; ?>" role="button" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>

                        <?php 
                                }
                            }
                        ?>

                    </tbody>
                    <?php
                        if ($cekBarang>0) {

                            $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$_SESSION[id_akun]'";
                            $prosesJenisAkun  = mysqli_query($koneksinya, $queryJenisAkun);
                            $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                            if ($resultJenisAkun['jenis_akun']=="Member") {
                                $hargaAwal      = $totalBayar;
                                $hargaDiskon    = (($hargaAwal*10)/100);
                                $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                $diskon         = "10%";
                            }else{
                                $hargaAkhir     = $totalBayar;
                                $diskon         = "0%";
                            }

                    ?>
                    <tfoot>
                        <tr>
                            <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                            <th scope="col" colspan="2" class="text-center">
                                <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                <small class="text-danger">Disc. 10%</small>  
                                <?php endif ?>

                                <h3>Rp<?= rp($hargaAkhir); ?></h3>
                            </th>
                        </tr>
                    </tfoot> 
                    <?php
                        }
                    ?>
                </table>

                <?php if ($cekBarang>0): ?>
                    <?php if (empty($_POST)): ?>
                        <form action="" method="POST" class="d-grid gap-2">
                            <button type="submit" name="checkout" class="btn btn-lg btn-primary rounded-0">CHECKOUT <i class="fa fa-arrow-right"></i></button>
                        </form>
                    <?php elseif (isset($_POST['checkout'])): ?>
                        <form action="" method="POST" class="d-grid gap-2 my-4 p-2 border border-primary">
                            <label for="pilih_metode_pembayaran" class="text-primary fw-bold">Pilih Metode Pembayaran</label>
                            <select class="form-select form-select-lg mb-3" id="pilih_metode_pembayaran" name="pilih_metode_pembayaran">
                                <option value="Online">Online</option>
                                <option value="Offline">Offline</option>
                            </select>
                            <button type="submit" name="metode_pembayaran" class="btn btn-lg btn-primary rounded-0">LANJUT <i class="fa fa-arrow-right"></i></button>
                        </form>
                    <?php elseif (isset($_POST['metode_pembayaran'])): ?>
                        <?php if ($_POST['pilih_metode_pembayaran']=='Online'): ?>
                            <form action="pages/proses-invoice.php" method="POST" enctype="multipart/form-data" class="d-grid gap-2 my-4 p-2 border border-primary">
                                <input type="hidden" name="kode_invoice" value="<?= $kode_invoiceNya; ?>">
                                <input type="hidden" name="diskon" value="<?= $diskon; ?>">
                                <input type="hidden" name="total_bayar" value="<?= $hargaAkhir; ?>">
                                <input type="hidden" name="jenis_pembelian" value="<?= $_POST['pilih_metode_pembayaran']; ?>">
                                <input type="hidden" name="status" value="Pending">
                                <label for="bukti_pembayaran" class="text-primary fw-bold">Upload Bukti Pembayaran</label>
                                <div class="input-group mb-3">
                                    <input type="file" class="form-control" id="bukti_pembayaran" name="bukti_pembayaran" accept="image/jpeg, image/jpg, image/png, image/gif" required>
                                    <label class="input-group-text" for="inputGroupFile02">Pilih Gambar</label>
                                </div>
                                <button type="submit" name="proses" class="btn btn-lg btn-primary rounded-0">LANJUT CHECKOUT <i class="fa fa-arrow-right"></i></button>
                            </form>
                        <?php else: ?>
                            <form action="pages/proses-invoice.php" method="POST" class="d-grid gap-2 my-4 p-2 border border-primary">
                                <input type="hidden" name="kode_invoice" value="<?= $kode_invoiceNya; ?>">
                                <input type="hidden" name="diskon" value="<?= $diskon; ?>">
                                <input type="hidden" name="total_bayar" value="<?= $hargaAkhir; ?>">
                                <input type="hidden" name="jenis_pembelian" value="<?= $_POST['pilih_metode_pembayaran']; ?>">
                                <input type="hidden" name="status" value="Belum Di Ambil">
                                <button type="submit" name="proses" class="btn btn-lg btn-primary rounded-0">LANJUT CHECKOUT <i class="fa fa-arrow-right"></i></button>
                            </form>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
            </div>

        </div>
    </div>
</section>