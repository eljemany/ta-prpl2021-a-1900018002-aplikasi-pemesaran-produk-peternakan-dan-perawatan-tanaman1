from kanren.facts import Relation, facts, fact
from kanren.core import var, run, conde
from kanren.goals import membero
from kanren import vars

ukuran = Relation()
warna = Relation()
gelap = Relation()

facts(ukuran, ("beruang", "besar"),
                ("gajah", "besar"),
                ("kucing", "kecil"))

facts(warna, ("beruang", "cokelat"),
             ("kucing", "hitam"),
             ("gajah", "kelabu"))

fact(gelap, "hitam")
fact(gelap, "cokelat")

x = var()
y = var()

kecil = run(0, x, ukuran(x, "kecil"))
print("\nHewan berukuran kecil : ", kecil)

besar = run(0, x, ukuran(x, "besar"))
print("\nHewan berukuran besar : ", besar)

cokelat = run(0, x, warna(x, "cokelat"))
print("\nHewan yang berwarna cokelat : ", cokelat)

q1 = run(0, x, membero(x, besar), membero(x, cokelat))
print("\nHewan besar dan berwarna cokelat : ", q1)

q2 = run(0, x, gelap(y), warna(x, y))
print("\nHewan berwana gelap : ", q2)

q3 = run(1, x, membero(x, besar), membero(y, q2))
print("\nHewan berukuran besar dan berwarna gelap : ", q3)

jenis = Relation()

facts(jenis, ("beruang", "karnivora"),
             ("kucing", "karnivora"))
x = var()

jenisBinatang = "karnivora"
binatang = run(0, x, jenis(x, jenisBinatang))
print("\nBinatang berjenis "+jenisBinatang+ " : ", binatang)