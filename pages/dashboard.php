<?php

    if (isset($_POST['submit'])) {

        $kode_invoice   = $_POST['kode_invoice'];
        $status         = "Sukses";

        $queryEDIT  = "UPDATE invoice SET status='$status' WHERE kode_invoice='$kode_invoice' ";
        $prosesEDIT = mysqli_query($koneksinya, $queryEDIT);

        if (!empty($prosesEDIT)) {
            echo "<script>window.alert('Barang berhasil di terima!');</script>";
        }
    }

?>

<!-- Section-->
<section class="py-5">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-10 p-4 my-4 border border-primary shadow rounded">
                <h2 class="border-bottom border-primary">PESANAN ONLINE SAYA</h2>
                <table class="table table-dark table-striped mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">INVOICE</th>
                            <th scope="col">Diskon</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            $no=1;
                            $queryOnline    = "SELECT * FROM invoice WHERE id_akun='$_SESSION[id_akun]' AND jenis_pembelian='Online' AND status!='Sukses' ORDER BY status ASC";
                            $prosesOnline   = mysqli_query($koneksinya, $queryOnline);
                            $cekOnline      = mysqli_num_rows($prosesOnline);
                            if ($cekOnline<=0) {
                        ?>

                        <tr class="text-center">
                            <td scope="row" colspan="6"><i>Tidak ada data ...</i></td>
                        </tr>

                        <?php
                            }else{
                                while ($resultOnline   = mysqli_fetch_assoc($prosesOnline)) {

                        ?>
                        
                        <tr>
                            <th scope="row"><?= $no++; ?></th>
                            <td><h5 class="text-primary border border-primary p-1"><?= $resultOnline['kode_invoice']; ?></h5></td>
                            <td><?= $resultOnline['diskon']; ?></td>
                            <td>Rp<?= rp($resultOnline['total_bayar']); ?></td>
                            <td>
                                <?php if ($resultOnline['status']=="Pending"): ?>
                                    <button type="button" class="btn btn-warning"><i class="fa fa-clock-o" aria-hidden="true"></i> Pending</button>
                                <?php elseif ($resultOnline['status']=="Pengiriman"): ?>
                                    <button type="button" class="btn btn-info"><i class="fa fa-truck" aria-hidden="true"></i> Pengiriman</button>
                                <?php elseif ($resultOnline['status']=="Sukses"): ?>
                                    <button type="button" class="btn btn-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Selesai</button>
                                <?php endif ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#detail<?= $no; ?>">
                                    <i class="fa fa-external-link"></i> Detail
                                </button>
                                <?php if ($resultOnline['status']=="Pengiriman"): ?>
                                    <form action="" method="POST" class="mt-2">
                                        <input type="hidden" name="kode_invoice" value="<?= $resultOnline['kode_invoice']; ?>">
                                        <button type="submit" name="submit" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#detail<?= $no; ?>">
                                            <i class="fa fa-check" aria-hidden="true"></i> Terima Barang
                                        </button>
                                    </form>
                                <?php endif ?>

                                <!-- Modal -->
                                <div class="modal fade" id="detail<?= $no; ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-primary" id="staticBackdropLabel">Detail Invoice: <u><?= $resultOnline['kode_invoice']; ?></u></h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">

                                                <table class="table table-dark table-striped mt-4">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</th>
                                                            <th scope="col">Nama Barang</th>
                                                            <th scope="col">Harga</th>
                                                            <th scope="col">Qty</th>
                                                            <th scope="col">Sub Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $no=1;
                                                            $totalBayar=0;
                                                            $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE kode_invoice='$resultOnline[kode_invoice]' ORDER BY id_keranjang DESC";
                                                            $prosesBarang   = mysqli_query($koneksinya, $queryBarang);
                                                            $cekBarang      = mysqli_num_rows($prosesBarang);
                                                            while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                                                $totalBayar+=$resultBarang['sub_jumlah'];
                                                        ?>
                                
                                                        <tr>
                                                            <th scope="row"><?= $no++; ?></th>
                                                            <td><?= $resultBarang['nama_barang']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                                            <td><?= $resultBarang['qty']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                    <?php
                                                        if ($cekBarang>0) {

                                                            $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$_SESSION[id_akun]'";
                                                            $prosesJenisAkun  = mysqli_query($koneksinya, $queryJenisAkun);
                                                            $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                                                            if ($resultJenisAkun['jenis_akun']=="Member") {
                                                                $hargaAwal      = $totalBayar;
                                                                $hargaDiskon    = (($hargaAwal*10)/100);
                                                                $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                                                $diskon         = "10%";
                                                            }else{
                                                                $hargaAkhir     = $totalBayar;
                                                                $diskon         = "0%";
                                                            }

                                                    ?>
                                                    <tfoot>
                                                        <tr>
                                                            <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                                                            <th scope="col" colspan="2" class="text-center">
                                                                <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                                                <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                                                <small class="text-danger">Disc. 10%</small>  
                                                                <?php endif ?>

                                                                <h3>Rp<?= rp($hargaAkhir); ?></h3>
                                                            </th>
                                                        </tr>
                                                    </tfoot> 
                                                    <?php
                                                        }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <?php 
                                }
                            }
                        ?>

                    </tbody>
                </table>
            </div>

            <div class="col-10 p-4 my-4 border border-primary shadow rounded">
                <h2 class="border-bottom border-primary">PESANAN OFFLINE SAYA</h2>
                <table class="table table-dark table-striped mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">INVOICE</th>
                            <th scope="col">Diskon</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            $no=1;
                            $queryOffline    = "SELECT * FROM invoice WHERE id_akun='$_SESSION[id_akun]' AND jenis_pembelian='Offline' AND status!='Sukses' ORDER BY status ASC";
                            $prosesOffline   = mysqli_query($koneksinya, $queryOffline);
                            $cekOffline      = mysqli_num_rows($prosesOffline);
                            if ($cekOffline<=0) {
                        ?>

                        <tr class="text-center">
                            <td scope="row" colspan="6"><i>Tidak ada data ...</i></td>
                        </tr>

                        <?php
                            }else{
                                while ($resultOffline   = mysqli_fetch_assoc($prosesOffline)) {

                        ?>
                        
                        <tr>
                            <th scope="row"><?= $no++; ?></th>
                            <td><h5 class="text-primary border border-primary p-1"><?= $resultOffline['kode_invoice']; ?></h5></td>
                            <td><?= $resultOffline['diskon']; ?></td>
                            <td>Rp<?= rp($resultOffline['total_bayar']); ?></td>
                            <td>
                                <?php if ($resultOffline['status']=="Belum Di Ambil"): ?>
                                    <button type="button" class="btn btn-warning"><i class="fa fa-clock-o" aria-hidden="true"></i> Belum Di Ambil</button>
                                <?php endif ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#detail<?= $no; ?>">
                                    <i class="fa fa-external-link"></i> Detail
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="detail<?= $no; ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-primary" id="staticBackdropLabel">Detail Invoice: <u><?= $resultOffline['kode_invoice']; ?></u></h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">

                                                <table class="table table-dark table-striped mt-4">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</th>
                                                            <th scope="col">Nama Barang</th>
                                                            <th scope="col">Harga</th>
                                                            <th scope="col">Qty</th>
                                                            <th scope="col">Sub Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $no=1;
                                                            $totalBayar=0;
                                                            $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE kode_invoice='$resultOffline[kode_invoice]' ORDER BY id_keranjang DESC";
                                                            $prosesBarang   = mysqli_query($koneksinya, $queryBarang);
                                                            $cekBarang      = mysqli_num_rows($prosesBarang);
                                                            while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                                                $totalBayar+=$resultBarang['sub_jumlah'];
                                                        ?>
                                
                                                        <tr>
                                                            <th scope="row"><?= $no++; ?></th>
                                                            <td><?= $resultBarang['nama_barang']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                                            <td><?= $resultBarang['qty']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                    <?php
                                                        if ($cekBarang>0) {

                                                            $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$_SESSION[id_akun]'";
                                                            $prosesJenisAkun  = mysqli_query($koneksinya, $queryJenisAkun);
                                                            $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                                                            if ($resultJenisAkun['jenis_akun']=="Member") {
                                                                $hargaAwal      = $totalBayar;
                                                                $hargaDiskon    = (($hargaAwal*10)/100);
                                                                $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                                                $diskon         = "10%";
                                                            }else{
                                                                $hargaAkhir     = $totalBayar;
                                                                $diskon         = "0%";
                                                            }

                                                    ?>
                                                    <tfoot>
                                                        <tr>
                                                            <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                                                            <th scope="col" colspan="2" class="text-center">
                                                                <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                                                <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                                                <small class="text-danger">Disc. 10%</small>  
                                                                <?php endif ?>

                                                                <h3>Rp<?= rp($hargaAkhir); ?></h3>
                                                            </th>
                                                        </tr>
                                                    </tfoot> 
                                                    <?php
                                                        }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <?php 
                                }
                            }
                        ?>

                    </tbody>
                </table>
            </div>

            <div class="col-10 p-4 my-4 border border-primary shadow rounded">
                <h2 class="border-bottom border-primary">RIWAYAT PESANAN SAYA</h2>
                <table class="table table-dark table-striped mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">INVOICE</th>
                            <th scope="col">Diskon</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            $no=1;
                            $queryRiwayat    = "SELECT * FROM invoice WHERE id_akun='$_SESSION[id_akun]' AND status='Sukses'";
                            $prosesRiwayat   = mysqli_query($koneksinya, $queryRiwayat);
                            $cekRiwayat      = mysqli_num_rows($prosesRiwayat);
                            if ($cekRiwayat<=0) {
                        ?>

                        <tr class="text-center">
                            <td scope="row" colspan="6"><i>Tidak ada data ...</i></td>
                        </tr>

                        <?php
                            }else{
                                while ($resultRiwayat   = mysqli_fetch_assoc($prosesRiwayat)) {

                        ?>
                        
                        <tr>
                            <th scope="row"><?= $no++; ?></th>
                            <td><h5 class="text-primary border border-primary p-1"><?= $resultRiwayat['kode_invoice']; ?></h5></td>
                            <td><?= $resultRiwayat['diskon']; ?></td>
                            <td>Rp<?= rp($resultRiwayat['total_bayar']); ?></td>
                            <td>
                                <?php if ($resultRiwayat['status']=="Pending"): ?>
                                    <button type="button" class="btn btn-warning"><i class="fa fa-clock-o" aria-hidden="true"></i> Pending</button>
                                <?php elseif ($resultRiwayat['status']=="Pengiriman"): ?>
                                    <button type="button" class="btn btn-info"><i class="fa fa-truck" aria-hidden="true"></i> Pengiriman</button>
                                <?php elseif ($resultRiwayat['status']=="Sukses"): ?>
                                    <button type="button" class="btn btn-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Selesai</button>
                                <?php endif ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#detail<?= $no; ?>">
                                    <i class="fa fa-external-link"></i> Detail
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="detail<?= $no; ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-primary" id="staticBackdropLabel">Detail Invoice: <u><?= $resultRiwayat['kode_invoice']; ?></u></h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">

                                                <table class="table table-dark table-striped mt-4">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</th>
                                                            <th scope="col">Nama Barang</th>
                                                            <th scope="col">Harga</th>
                                                            <th scope="col">Qty</th>
                                                            <th scope="col">Sub Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $no=1;
                                                            $totalBayar=0;
                                                            $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE kode_invoice='$resultRiwayat[kode_invoice]' ORDER BY id_keranjang DESC";
                                                            $prosesBarang   = mysqli_query($koneksinya, $queryBarang);
                                                            $cekBarang      = mysqli_num_rows($prosesBarang);
                                                            while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                                                $totalBayar+=$resultBarang['sub_jumlah'];
                                                        ?>
                                
                                                        <tr>
                                                            <th scope="row"><?= $no++; ?></th>
                                                            <td><?= $resultBarang['nama_barang']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                                            <td><?= $resultBarang['qty']; ?></td>
                                                            <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                    <?php
                                                        if ($cekBarang>0) {

                                                            $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$_SESSION[id_akun]'";
                                                            $prosesJenisAkun  = mysqli_query($koneksinya, $queryJenisAkun);
                                                            $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                                                            if ($resultJenisAkun['jenis_akun']=="Member") {
                                                                $hargaAwal      = $totalBayar;
                                                                $hargaDiskon    = (($hargaAwal*10)/100);
                                                                $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                                                $diskon         = "10%";
                                                            }else{
                                                                $hargaAkhir     = $totalBayar;
                                                                $diskon         = "0%";
                                                            }

                                                    ?>
                                                    <tfoot>
                                                        <tr>
                                                            <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                                                            <th scope="col" colspan="2" class="text-center">
                                                                <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                                                <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                                                <small class="text-danger">Disc. 10%</small>  
                                                                <?php endif ?>

                                                                <h3>Rp<?= rp($hargaAkhir); ?></h3>
                                                            </th>
                                                        </tr>
                                                    </tfoot> 
                                                    <?php
                                                        }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <?php 
                                }
                            }
                        ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>