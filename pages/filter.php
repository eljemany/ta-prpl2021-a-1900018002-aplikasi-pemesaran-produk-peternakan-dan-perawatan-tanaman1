<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-2">
        <h3 class="border p-4">Result by: <u><?= $_GET['result_by']; ?></u></h3>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center mt-4">

            <?php

                if ($_GET['result_by']=='Produk Terbaru') {
                    $queryBarang  = "SELECT * FROM barang INNER JOIN kategori ON barang.id_kategori = kategori.id_kategori ORDER BY id_barang DESC";
                }elseif ($_GET['result_by']=='Produk Termurah') {
                    $queryBarang  = "SELECT * FROM barang INNER JOIN kategori ON barang.id_kategori = kategori.id_kategori ORDER BY harga ASC";
                }elseif ($_GET['result_by']=='Produk Termahal') {
                    $queryBarang  = "SELECT * FROM barang INNER JOIN kategori ON barang.id_kategori = kategori.id_kategori ORDER BY harga DESC";
                }

                $prosesBarang = mysqli_query($koneksinya, $queryBarang);
                while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {

            ?>

            <div class="col mb-5">
                <div class="card h-100">
                    <!-- Sale badge-->
                    <?php if ($resultBarang['status']=='Pre-Order'): ?>
                        <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Pre Order</div>
                    <?php endif ?>
                    <!-- Product image-->
                    <img class="card-img-top border-bottom p-1" src="assets/barang/<?= $resultBarang['gambar']; ?>" alt="<?= $resultBarang['nama_barang']; ?>" />
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder"><?= $resultBarang['nama_barang']; ?></h5>
                            <!-- Product reviews-->
                            <div class="d-flex justify-content-center small text-warning mb-2">
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                            </div>
                            <!-- Product price-->
                            <p>Rp<?= rp($resultBarang['harga']); ?></p>
                            <span>Stok: <?= rp($resultBarang['stock']); ?></span>
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="col-12 p-4 pt-0 border-top-0 bg-transparent">

                        <div class="text-center">
                            <button type="button" class="btn btn-dark mb-2" data-bs-toggle="modal" data-bs-target="#detail<?= $resultBarang['id_barang']; ?>">
                                <i class="fa fa-external-link"></i> Detail
                            </button>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="detail<?= $resultBarang['id_barang']; ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Detail <?= $resultBarang['nama_barang']; ?></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <?= $resultBarang['deskripsi']; ?>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="text-center">
                                            <button class="btn btn-block btn-outline-dark" data-bs-toggle="modal" data-bs-target="#keranjang<?= $resultBarang['id_barang']; ?>">
                                                <i class="fa fa-cart-plus"></i> Masukkan ke Keranjang
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-block btn-outline-dark" data-bs-toggle="modal" data-bs-target="#keranjang<?= $resultBarang['id_barang']; ?>">
                                <i class="fa fa-cart-plus"></i> Keranjang
                            </button>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="keranjang<?= $resultBarang['id_barang']; ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <form action="pages/proses-tambah-keranjang.php" method="POST" class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Masukkan jumlah pembelian</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id_barang" value="<?= $resultBarang['id_barang']; ?>">
                                        <input type="hidden" name="stock" value="<?= $resultBarang['stock']; ?>">
                                        <input type="hidden" name="harga" value="<?= $resultBarang['harga']; ?>">
                                        <input type="number" class="form-control" name="qty" placeholder="Masukkan jumlah pembelian anda" value="1" min="1" max="<?= $resultBarang['stock']; ?>" required>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="d-grid gap-2 text-center">
                                            <button type="submit" name="submit" class="btn btn-lg btn-outline-dark" data-bs-toggle="modal" data-bs-target="#detail<?= $resultBarang['id_barang']; ?>">
                                                <i class="fa fa-cart-plus"></i> Masukkan ke Keranjang
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php } ?>

        </div>
    </div>
</section>