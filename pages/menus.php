<?php

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['nama_akun']) AND empty($_SESSION['jenis_akun'])) {
        $totalKeranjang = 0;
    }else{
        $queryTotalKeranjang  = "SELECT SUM(qty) AS TotalKeranjang FROM keranjang WHERE id_akun='$_SESSION[id_akun]' AND status='Keranjang' ";
        $prosesTotalKeranjang = mysqli_query($koneksinya, $queryTotalKeranjang);
        $resultTotalKeranjang = mysqli_fetch_assoc($prosesTotalKeranjang);

        if ($resultTotalKeranjang['TotalKeranjang']===NULL) {
            $totalKeranjang = 0;
        }else{
            $totalKeranjang = $resultTotalKeranjang['TotalKeranjang'];
        }
    }

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="index.php">Store<strong>Peternakan</strong></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link <?php if($_GET['pages']=='Home'){ echo 'active'; } ?>" aria-current="page" href="index.php"><i class="fa fa-home"></i> Home</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link <?php if($_GET['pages']=='Kategori'){ echo 'active'; } ?> dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-list-ul"></i> Kategori</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item <?php if($_GET['pages']=='Kategori' AND $_GET['id']=='all'){ echo 'bg-dark text-light'; } ?>" href="primary-pages.php?pages=Kategori&id=all&nama_kategori=All">Lihat Semua Kategori</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <?php

                            $queryKategori  = "SELECT * FROM kategori ORDER BY nama_kategori ASC";
                            $prosesKategori = mysqli_query($koneksinya, $queryKategori);
                            while ($resultKategori   = mysqli_fetch_assoc($prosesKategori)) {

                        ?>
                        <li><a class="dropdown-item <?php if($_GET['pages']=='Kategori' AND $_GET['id']==$resultKategori['id_kategori']){ echo 'bg-dark text-light'; } ?>" href="primary-pages.php?pages=Kategori&id=<?= $resultKategori['id_kategori']; ?>&nama_kategori=<?= $resultKategori['nama_kategori']; ?>"><?= $resultKategori['nama_kategori']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link <?php if($_GET['pages']=='Filter'){ echo 'active'; } ?> dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-filter"></i> Filter</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item <?php if($_GET['pages']=='Filter' AND $_GET['result_by']=='Produk Terbaru'){ echo 'bg-dark text-light'; } ?>" href="primary-pages.php?pages=Filter&result_by=Produk%20Terbaru">Produk Terbaru</a></li>
                        <li><a class="dropdown-item <?php if($_GET['pages']=='Filter' AND $_GET['result_by']=='Produk Termurah'){ echo 'bg-dark text-light'; } ?>" href="primary-pages.php?pages=Filter&result_by=Produk%20Termurah">Produk Termurah</a></li>
                        <li><a class="dropdown-item <?php if($_GET['pages']=='Filter' AND $_GET['result_by']=='Produk Termahal'){ echo 'bg-dark text-light'; } ?>" href="primary-pages.php?pages=Filter&result_by=Produk%20Termahal">Produk Termahal</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex">
                <a href="third-pages.php?pages=Keranjang" class="btn btn-outline-dark mx-2">
                    <i class="fa fa-shopping-cart"></i>
                    Keranjang
                    <span class="badge bg-dark text-white ms-1 rounded-pill"><?= $totalKeranjang; ?></span>
                </a>
                <a href="third-pages.php?pages=Dashboard" class="btn btn-dark">
                    <i class="fa fa-user"></i>
                    Akun Saya
                </a>
                <?php if (!empty($_SESSION['id_akun']) AND !empty($_SESSION['nama_akun']) AND !empty($_SESSION['jenis_akun'])): ?>
                    <a href="logout.php" class="btn btn-outline-dark mx-2">
                        <i class="fa fa-sign-out"></i>
                    </a>
                <?php endif ?>
            </form>
        </div>
    </div>
</nav>