<?php

    require '../koneksinya.php';
    require '../functions.php';

    session_start();
    if (empty($_SESSION['id_akun']) AND empty($_SESSION['nama_akun']) AND empty($_SESSION['jenis_akun'])) {
        echo "<script>window.alert('Anda belum login! Silahkan login terlebih dahulu!'); location.href = '../secondary-pages.php?pages=Login';</script>";
    }else{
        if (isset($_POST['proses'])) {

            $kode_invoice       = $_POST['kode_invoice'];
            $id_akun            = $_SESSION['id_akun'];
            $diskon             = $_POST['diskon'];
            $total_bayar        = $_POST['total_bayar'];
            $jenis_pembelian    = $_POST['jenis_pembelian'];
            $status             = $_POST['status'];

            if ($jenis_pembelian=="Online") {
                // Include gambar
                $lokasi_file            = $_FILES['bukti_pembayaran']['tmp_name'];
                $lokasi_upload          = "../assets/bukti-pembayaran/";
                $nama_file              = $_FILES['bukti_pembayaran']['name'];
                $tipe_file              = strtolower($_FILES['bukti_pembayaran']['type']);
                $tipe_file2             = format_gambar($tipe_file); // ngedapetin png / jpg / jpeg
                $acak                   = rand(00,99);
                $nama_file_unik         = "bukti-pembayaran-".seo($kode_invoice)."-".seo($_SESSION['nama_akun'])."-".$acak.".".$tipe_file2;

                uploadGambarAsli($nama_file_unik, $tipe_file, $lokasi_file, $lokasi_upload);
            }else{
                $nama_file_unik = NULL;
            }

            $queryAdd   = "INSERT INTO invoice VALUES ('$kode_invoice', '$id_akun', '$diskon', '$total_bayar', '$nama_file_unik', '$jenis_pembelian', '$status')";
            $prosesAdd  = mysqli_query($koneksinya, $queryAdd);

            if (!empty($prosesAdd)) {
                $queryEdit   = "UPDATE keranjang SET status='Checkout' WHERE kode_invoice='$kode_invoice' ";
                $prosesEdit  = mysqli_query($koneksinya, $queryEdit);

                if (!empty($prosesEdit)) {
                    echo "<script>window.alert('Berhasil!'); location.href = '../third-pages.php?pages=Keranjang';</script>";
                }else{
                    exit();
                    echo "<script>window.alert('GAGAL!'); window.location(history.back(-1))</script>";
                }
            }else{
                exit();
                echo "<script>window.alert('GAGAL!'); window.location(history.back(-1))</script>";
            }
        }
    }

?>