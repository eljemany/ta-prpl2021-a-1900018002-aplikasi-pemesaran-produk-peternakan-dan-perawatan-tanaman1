<?php

    if (isset($_POST['submit'])) {
        $username   = $_POST['username'];
        $password   = $_POST['password'];

        $queryLoginAkun    = "SELECT * FROM akun WHERE username='$username' AND password='$password' ";
        $prosesLoginAkun   = mysqli_query($koneksinya, $queryLoginAkun);
        $resultLoginAkun   = mysqli_fetch_assoc($prosesLoginAkun);

        if (!empty($resultLoginAkun)) {
            session_start();
            $_SESSION['id_akun']    = $resultLoginAkun['id_akun'];
            $_SESSION['nama_akun']  = $resultLoginAkun['nama_akun'];
            $_SESSION['jenis_akun'] = $resultLoginAkun['jenis_akun'];
            $_SESSION['username']   = $resultLoginAkun['username'];
            $_SESSION['password']   = $resultLoginAkun['password'];

            header('location:third-pages.php?pages=Dashboard');
        }else{
            echo "<script>window.alert('Username atau Password yang anda masukkan salah!'); window.location(history.back(-1))</script>";
        }
    }

?>

<header class="bg-dark py-5">
    <div class="container px-4 px-lg-4 my-4">
        <div class="row justify-content-center">
            <h3 class="text-center text-light mb-4">PORTAL LOGIN AKUN</h3>
            <div class="col-6 p-4 border">
                <form action="" method="POST" class="text-light">
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username anda" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password anda" required>
                    </div>
                    <div class="d-grid gap-2 text-center">
                        <button type="submit" name="submit" class="btn btn-outline-primary">MASUK <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <p class="text-muted">Tidak punya akun?</p>
                        <a href="secondary-pages.php?pages=Register" class="btn btn-primary">YUK DAFTAR <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>