<?php

    if (isset($_POST['submit'])) {
        $nama_akun  = $_POST['nama_akun'];
        $jenis_akun = $_POST['jenis_akun'];
        $username   = $_POST['username'];
        $password   = $_POST['password'];

        $queryAddAkun    = "INSERT INTO akun VALUES ('', '$nama_akun', '$jenis_akun', '$username', '$password')";
        $prosesAddAkun   = mysqli_query($koneksinya, $queryAddAkun);

        if (!empty($prosesAddAkun)) {
            echo "<script>window.alert('Berhasil! Silahkan login'); location.href = 'secondary-pages.php?pages=Login';</script>";
        }else{
            echo "<script>window.alert('GAGAL!'); window.location(history.back(-1))</script>";
        }
    }

?>

<header class="bg-dark py-5">
    <div class="container px-4 px-lg-4 my-4">
        <div class="row justify-content-center">
            <h3 class="text-center text-light mb-4">FORM PENDAFTARAN AKUN</h3>
            <div class="col-6 p-4 border">
                <form action="" method="POST" class="text-light">
                    <div class="mb-3">
                        <label for="nama_akun" class="form-label">Nama Akun</label>
                        <input type="text" class="form-control" id="nama_akun" name="nama_akun" placeholder="Masukkan Nama Akun anda" required>
                    </div>
                    <div class="mb-3">
                        <label for="jenis_akun" class="form-label">Jenis Akun</label>
                        <select class="form-select" id="jenis_akun" name="jenis_akun" aria-label="jenis_akun">
                            <option value="Customer Biasa">Customer Biasa</option>
                            <option value="Member">Member</option>
                            <option value="Reseller">Reseller</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username anda" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password anda" required>
                    </div>
                    <div class="d-grid gap-2 text-center">
                        <button type="submit" name="submit" class="btn btn-outline-primary">DAFTAR <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        <p class="text-muted">Sudah punya akun?</p>
                        <a href="secondary-pages.php?pages=Login" class="btn btn-primary">MASUK <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>