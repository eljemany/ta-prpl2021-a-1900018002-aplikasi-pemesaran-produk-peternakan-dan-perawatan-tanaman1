<?php

    require 'koneksinya.php';
    require 'functions.php';

    session_start();
    if (empty($_SESSION['id_akun']) AND empty($_SESSION['nama_akun']) AND empty($_SESSION['jenis_akun'])) {
        header("Location: secondary-pages.php?pages=Login");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title><?= $_GET['pages']; ?> - Store Peternakan</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />

    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body>
    <!-- Navigation-->
    <?php require 'pages/menus.php'; ?>
    <!-- Header-->

    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-4 my-4">
            <div class="text-center text-white">
                <h4>
                    <figcaption class="blockquote-footer">
                        <?= $_GET['pages']; ?>
                    </figcaption>
                </h4>
                <h1 class="display-4 fw-bolder"><?= $_SESSION['nama_akun']; ?></h1>
            </div>
        </div>
    </header>

    <!-- Konten -->
    <?php

        if ($_GET['pages']=='Dashboard') {
            require 'pages/dashboard.php';
        }elseif ($_GET['pages']=='Keranjang') {
            require 'pages/keranjang.php';
        }

    ?>
    <!-- End Konten -->

    <!-- Footer-->
    <footer class="py-5 bg-dark">
        <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Store Peternakan 2021</p></div>
    </footer>

    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>
</html>
