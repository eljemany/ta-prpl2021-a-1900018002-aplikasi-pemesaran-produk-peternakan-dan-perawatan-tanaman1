<?php

	session_start();
	unset($_SESSION['id_akun']);
	unset($_SESSION['nama_akun']);
	unset($_SESSION['jenis_akun']);
	session_unset();
	session_destroy();

	header("Location: primary-pages.php?pages=Home");