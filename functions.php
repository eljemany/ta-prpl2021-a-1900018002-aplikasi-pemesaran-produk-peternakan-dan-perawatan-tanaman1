<?php

	function rp($angka){
	  	$rupiah=number_format($angka,0,',','.');
	  	return $rupiah;
	}

	function seo($s) {
	    $c = array (' ');
	    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','“','”');

	    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
	    
	    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
	    return $s;
	}

	function format_gambar($s) {
	    $c = array (' ');
	    $d = array ('/','image');

	    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
	    
	    $s = strtolower(str_replace($c, '', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
	    return $s;
	}

	function uploadGambarWidth450x300($name_file, $type_file, $location_file, $location_upload){
	
		//direktori gambar
		$vfile_upload 		= $location_upload.$name_file;

		// Simpan gambar dalam ukuran sebenarnya
		move_uploaded_file($location_file, $vfile_upload);

		//identitas file asli
		if ($type_file=="image/jpeg" ){
			$im_src = imagecreatefromjpeg($vfile_upload);
		}elseif ($type_file=="image/jpg" ){
			$im_src = imagecreatefromjpg($vfile_upload);
		}elseif ($type_file=="image/png" ){
			$im_src = imagecreatefrompng($vfile_upload);
		}elseif ($tipe_file=="image/gif" ){
			$im_src = imagecreatefromgif($vfile_upload);
	    }elseif ($tipe_file=="image/wbmp" ){
			$im_src = imagecreatefromwbmp($vfile_upload);
	    }elseif ($tipe_file=="image/webp" ){
			$im_src = imagecreatefromwebp($vfile_upload);
	    }

		// Simpan gambar dalam ukuran yang di maksud
		$src_width = imageSX($im_src);
		$src_height = imageSY($im_src);
		
		if($src_width>450 OR $src_width<450){
			$dst_width = 450;
		}else{
			$dst_width = $src_width;
		}

		$dst_height = 300;

		$im = imagecreatetruecolor($dst_width,$dst_height);
		
		// Turn off transparency blending (temporarily)
		imagealphablending($im, false);
		// Create a new transparent color for image
		$color = imagecolorallocatealpha($im, 0, 0, 0, 127);
		// Completely fill the background of the new image with allocated color.
		imagefill($im, 0, 0, $color);
		// Restore transparency blending
		imagesavealpha($im, true);
		//0, 0, 0, 0 letak gambar
		imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

		if ($type_file=="image/jpeg" ){
			imagejpeg($im,$vfile_upload);
	    }elseif ($type_file=="image/jpg" ){
			imagejpg($im,$vfile_upload);
	    }elseif ($type_file=="image/png" ){
			imagepng($im,$vfile_upload);
	    }elseif ($type_file=="image/gif" ){
			imagegif($im,$vfile_upload);
	    }elseif ($type_file=="image/wbmp" ){
			imagewbmp($im,$vfile_upload);
	    }elseif ($type_file=="image/webp" ){
			imagewebp($im,$vfile_upload);
	    }
	  
		imagedestroy($im_src);
		imagedestroy($im);
	}

	function uploadGambarAsli($name_file, $type_file, $location_file, $location_upload){
		
		//direktori gambar
		$vfile_upload 	= $location_upload.$name_file;

		// Simpan gambar dalam ukuran sebenarnya
		move_uploaded_file($location_file, $vfile_upload);
		
	}