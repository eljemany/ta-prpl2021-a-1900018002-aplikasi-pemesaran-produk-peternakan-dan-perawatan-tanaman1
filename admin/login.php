<?php

	include 'functions/conn.php';

	if (isset($_POST['login'])) {
		$username 	= $_POST['username'];
		$password 	= $_POST['password'];

		$queryLoginAdmin	= "SELECT * FROM admin WHERE username='$username' AND password='$password' ";
		$prosesLoginAdmin	= mysqli_query($conn, $queryLoginAdmin);
        $resultLoginAdmin	= mysqli_fetch_assoc($prosesLoginAdmin);

        if (!empty($resultLoginAdmin)) {
            session_start();
            $_SESSION['id_admin']	= $resultLoginAdmin['id_admin'];
            $_SESSION['nama_admin']	= $resultLoginAdmin['nama_admin'];
            $_SESSION['username']	= $resultLoginAdmin['username'];
            $_SESSION['password']	= $resultLoginAdmin['password'];

            header('location:index.php?content=dashboard');
        }else{
        	echo "<script>window.alert('Username atau Password yang anda masukkan salah!'); window.location(history.back(-1))</script>";
        }
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Portal Admin - Store Peternakan</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="../css/font-awesome.min.css">
</head>
<body>

	<div class="container">
	    <div class="row py-5 mt-4 justify-content-center">

	        <!-- Registeration Form -->
	        <div class="col-md-7 col-lg-6 border shadow py-4">


		        <!-- For Demo Purpose -->
		        <div class="text-center border-bottom mb-4 pb-4">
		            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" alt="" class="img-fluid mb-3 d-none d-md-block">
		            <h1 class="text-success">Selamat Datang di Portal Admin - Store Peternakan</h1>
		            <p class="text-muted mb-0">Silahkan login menggunakan Username & Password yang anda punya</p>
		        </div>

	        	<ul class="nav nav-tabs mb-4" role="tablist">
				    <li class="nav-item">
				      	<a class="nav-link font-weight-bold active" data-toggle="tab" href="#pelamar"><i class="fa fa-sign-in"></i> Login Panel</a>
				    </li>
				</ul>

				<div class="tab-content">

		            <form action="" method="POST" id="pelamar" class="tab-pane active">
		                <div class="row">

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="username" placeholder="Username" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-key text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="password" name="password" placeholder="Password" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <!-- Submit Button -->
		                    <div class="form-group col-lg-12 mx-auto">
		                        <button type="submit" name="login" class="btn btn-success btn-block">
		                            <span class="font-weight-bold">LOGIN <i class="fa fa-sign-in"></i></span>
		                        </button>
		                    </div>

		                </div>
		            </form>

		        </div>

	        </div>
	    </div>
	</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>