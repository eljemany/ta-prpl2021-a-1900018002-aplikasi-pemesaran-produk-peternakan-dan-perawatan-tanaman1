<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?content=dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-smile"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Portal <sup>Admin</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item <?php if($_GET['content']=='dashboard'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu Utama
    </div>

    <li class="nav-item <?php if($_GET['content']=='kategori' OR $_GET['content']=='buat-kategori' OR $_GET['content']=='edit-kategori' OR $_GET['content']=='cek-pelamar'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=kategori">
            <i class="fas fa-clipboard-list"></i>
            <span>Manajemen Kategori</span></a>
    </li>

    <li class="nav-item <?php if($_GET['content']=='barang' OR $_GET['content']=='buat-barang' OR $_GET['content']=='edit-barang' OR $_GET['content']=='cek-pelamar'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=barang">
            <i class="fas fa-file-signature"></i>
            <span>Manajemen Barang</span></a>
    </li>

    <li class="nav-item <?php if($_GET['content']=='pesanan-online'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=pesanan-online">
            <i class="fas fa-cart-arrow-down"></i>
            <span>Pesanan Online</span></a>
    </li>

    <li class="nav-item <?php if($_GET['content']=='pesanan-offline'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=pesanan-offline">
            <i class="fas fa-cart-arrow-down"></i>
            <span>Pesanan Offline</span></a>
    </li>

    <li class="nav-item <?php if($_GET['content']=='rekap-penjualan'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=rekap-penjualan">
            <i class="fas fa-folder-open fa-2x text-gray-300"></i>
            <span>Rekap Penjualan</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>