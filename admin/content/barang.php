<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-file-signature"></i> Barang</h1>
        <a href="index.php?content=buat-barang" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Tambah Barang</a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th>Nama Barang</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                                <th>Stock</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryBarang  = "SELECT * FROM barang INNER JOIN kategori ON barang.id_kategori = kategori.id_kategori ORDER BY id_barang DESC";
                                $prosesBarang = mysqli_query($conn, $queryBarang);
                                while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $resultBarang['nama_barang']; ?></td>
                                <td><button type="button" class="btn btn-primary"><?= $resultBarang['nama_kategori']; ?></button></td>
                                <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                <td>
                                    <?php if ($resultBarang['stock']<=0): ?>
                                        <button type="button" class="btn btn-danger"><?= rp($resultBarang['stock']); ?></button>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-primary"><?= rp($resultBarang['stock']); ?></button>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <?php if ($resultBarang['status']=="Tersedia"): ?>
                                        <button type="button" class="btn btn-success"><?= $resultBarang['status']; ?></button>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-warning"><?= $resultBarang['status']; ?></button>
                                    <?php endif ?>
                                </td>
                                <td class="text-center">
                                    <a href="index.php?content=edit-barang&id=<?= $resultBarang['id_barang']; ?>" class="btn btn-info">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>