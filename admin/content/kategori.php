<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-clipboard-list"></i> Kategori</h1>
        <a href="index.php?content=buat-kategori" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Buat Kategori</a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th>Nama Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryLowongan  = "SELECT * FROM kategori ORDER BY id_kategori DESC";
                                $prosesLowongan = mysqli_query($conn, $queryLowongan);
                                while ($resultLowongan   = mysqli_fetch_assoc($prosesLowongan)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><button type="button" class="btn btn-primary"><?= $resultLowongan['nama_kategori']; ?></button></td>
                                <td class="text-center">
                                    <a href="index.php?content=edit-kategori&id=<?= $resultLowongan['id_kategori']; ?>" class="btn btn-info">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>