<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-folder-open"></i> Rekap Penjualan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%">No</th>
                                <th>INVOICE</th>
                                <th>Diskon</th>
                                <th>Total Bayar</th>
                                <th>Jenis Pembelian</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryInvoice  = "SELECT * FROM invoice WHERE status='Sukses' ORDER BY status ASC";
                                $prosesInvoice = mysqli_query($conn, $queryInvoice);
                                while ($resultInvoice   = mysqli_fetch_assoc($prosesInvoice)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><h5 class="text-primary border border-primary font-weight-bold p-1"><?= $resultInvoice['kode_invoice']; ?></h5></td>
                                <td><button type="button" class="btn btn-outline-danger"><?= $resultInvoice['diskon']; ?></button></td>
                                <td><strong>Rp<?= rp($resultInvoice['total_bayar']); ?></strong></td>
                                <td><?= $resultInvoice['jenis_pembelian']; ?></td>
                                <td>
                                    
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detail<?= $no; ?>">
                                        <i class="fas fa-external-link-alt"></i> Detail
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="detail<?= $no; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary" id="staticBackdropLabel">Detail Invoice: <u><?= $resultInvoice['kode_invoice']; ?></u></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <table class="table table-dark table-striped mt-4">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">No</th>
                                                                <th scope="col">Nama Barang</th>
                                                                <th scope="col">Harga</th>
                                                                <th scope="col">Qty</th>
                                                                <th scope="col">Sub Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                $no=1;
                                                                $totalBayar=0;
                                                                $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE kode_invoice='$resultInvoice[kode_invoice]' ORDER BY id_keranjang DESC";
                                                                $prosesBarang   = mysqli_query($conn, $queryBarang);
                                                                $cekBarang      = mysqli_num_rows($prosesBarang);
                                                                while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                                                    $totalBayar+=$resultBarang['sub_jumlah'];
                                                            ?>
                                    
                                                            <tr>
                                                                <th scope="row"><?= $no++; ?></th>
                                                                <td><?= $resultBarang['nama_barang']; ?></td>
                                                                <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                                                <td><?= $resultBarang['qty']; ?></td>
                                                                <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                        <?php
                                                            if ($cekBarang>0) {

                                                                $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$resultInvoice[id_akun]'";
                                                                $prosesJenisAkun  = mysqli_query($conn, $queryJenisAkun);
                                                                $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                                                                if ($resultJenisAkun['jenis_akun']=="Member") {
                                                                    $hargaAwal      = $totalBayar;
                                                                    $hargaDiskon    = (($hargaAwal*10)/100);
                                                                    $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                                                    $diskon         = "10%";
                                                                }else{
                                                                    $hargaAkhir     = $totalBayar;
                                                                    $diskon         = "0%";
                                                                }

                                                        ?>
                                                        <tfoot>
                                                            <tr>
                                                                <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                                                                <th scope="col" colspan="2" class="text-center">
                                                                    <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                                                    <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                                                    <small class="text-danger">Disc. 10%</small>  
                                                                    <?php endif ?>

                                                                    <h3>Rp<?= rp($hargaAkhir); ?></h3>
                                                                </th>
                                                            </tr>
                                                        </tfoot> 
                                                        <?php
                                                            }
                                                        ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>