<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard Admin Store Peternakan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <?php

            // REKAP BARANG

            $queryBarangTersedia    = "SELECT SUM(stock) AS barangTersedia FROM barang WHERE stock!='0'";
            $prosesBarangTersedia   = mysqli_query($conn, $queryBarangTersedia);
            $resultBarangTersedia   = mysqli_fetch_assoc($prosesBarangTersedia);

            $queryBarangHabis       = "SELECT COUNT(id_barang) AS barangHabis FROM barang WHERE stock='0'";
            $prosesBarangHabis      = mysqli_query($conn, $queryBarangHabis);
            $resultBarangHabis      = mysqli_fetch_assoc($prosesBarangHabis);

            $queryBarangTerjual     = "SELECT SUM(qty) AS barangTerjual FROM keranjang WHERE status='Checkout'";
            $prosesBarangTerjual    = mysqli_query($conn, $queryBarangTerjual);
            $resultBarangTerjual    = mysqli_fetch_assoc($prosesBarangTerjual);

            $queryPenghasilan       = "SELECT SUM(total_bayar) AS Penghasilan FROM invoice WHERE status='Sukses'";
            $prosesPenghasilan      = mysqli_query($conn, $queryPenghasilan);
            $resultPenghasilan      = mysqli_fetch_assoc($prosesPenghasilan);

            // REKAP AKUN

            $querySemuaAkun         = "SELECT COUNT(id_akun) AS semuaAkun FROM akun";
            $prosesSemuaAkun        = mysqli_query($conn, $querySemuaAkun);
            $resultSemuaAkun        = mysqli_fetch_assoc($prosesSemuaAkun);

            $queryCustomerBiasa     = "SELECT COUNT(id_akun) AS customerBiasa FROM akun WHERE jenis_akun='Customer Biasa'";
            $prosesCustomerBiasa    = mysqli_query($conn, $queryCustomerBiasa);
            $resultCustomerBiasa    = mysqli_fetch_assoc($prosesCustomerBiasa);

            $queryMember            = "SELECT COUNT(id_akun) AS Member FROM akun WHERE jenis_akun='Member'";
            $prosesMember           = mysqli_query($conn, $queryMember);
            $resultMember           = mysqli_fetch_assoc($prosesMember);

            $queryReseller          = "SELECT COUNT(id_akun) AS Reseller FROM akun WHERE jenis_akun='Reseller'";
            $prosesReseller         = mysqli_query($conn, $queryReseller);
            $resultReseller         = mysqli_fetch_assoc($prosesReseller);

        ?>

        <div class="col-md-12 mb-4 border-primary">
            <div class="row p-2 border border-primary bg-primary">
                <h3 class="border-bottom text-light">REKAP BARANG</h3>
            </div>
            <div class="row px-2 pt-4 border border-primary">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Barang Tersedia</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultBarangTersedia['barangTersedia']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="far fa-file-alt fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Barang Habis</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultBarangHabis['barangHabis']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-list-ul fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Barang Terjual</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultBarangTerjual['barangTerjual']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-check-double fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Penghasilan</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">Rp<?= rp($resultPenghasilan['Penghasilan']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-money-bill-alt fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 mb-4 border-primary">
            <div class="row p-2 border border-primary bg-primary">
                <h3 class="border-bottom text-light">REKAP AKUN</h3>
            </div>
            <div class="row px-2 pt-4 border border-primary">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-12 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Semua Akun</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultSemuaAkun['semuaAkun']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-users fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-4 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Akun Customer Biasa</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultCustomerBiasa['customerBiasa']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="far fa-user fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-4 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Akun Member</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultMember['Member']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-tie fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-md-4 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Akun Reseller</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rp($resultReseller['Reseller']); ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-tag fa-2x text-gray-500"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>