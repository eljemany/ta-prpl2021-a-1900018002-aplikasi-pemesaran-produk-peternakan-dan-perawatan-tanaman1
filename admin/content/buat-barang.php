<?php

    date_default_timezone_set("Asia/Jakarta");

    if (isset($_POST['selesai'])) {

        $id_kategori    = $_POST['id_kategori'];
        $nama_barang    = $_POST['nama_barang'];
        $harga          = $_POST['harga'];
        $stock          = $_POST['stock'];
        $deskripsi      = $_POST['deskripsi'];
        $status         = $_POST['status'];

        // Include gambar
        $lokasi_file            = $_FILES['gambar']['tmp_name'];
        $lokasi_upload          = "../assets/barang/";
        $nama_file              = $_FILES['gambar']['name'];
        $tipe_file              = strtolower($_FILES['gambar']['type']);
        $tipe_file2             = format_gambar($tipe_file); // ngedapetin png / jpg / jpeg
        $acak                   = rand(00,99);
        $nama_file_unik         = seo($nama_barang)."-".$acak.".".$tipe_file2;

        uploadGambarWidth450x300($nama_file_unik, $tipe_file, $lokasi_file, $lokasi_upload);

        $queryADD  = "INSERT INTO barang VALUES ('', '$id_kategori', '$nama_barang', '$harga', '$stock', '$nama_file_unik', '$deskripsi', '$status')";
        $prosesADD = mysqli_query($conn, $queryADD);

        if (!empty($prosesADD)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=barang';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Tambah Barang</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" enctype="multipart/form-data" class="col-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                            <hr>
                        </div>

                        <div class="form-group">
                            <label for="id_kategori">Pilih Kategori</label>
                            <select class="form-control" id="id_kategori" name="id_kategori">
                                <?php

                                    $queryKategori  = "SELECT * FROM kategori ORDER BY nama_kategori ASC";
                                    $prosesKategori = mysqli_query($conn, $queryKategori);
                                    while ($resultKategori   = mysqli_fetch_assoc($prosesKategori)) {
                                ?>
                                <option value="<?= $resultKategori['id_kategori']; ?>"><?= $resultKategori['nama_kategori']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" id="nama_barang" class="form-control form-control-user" name="nama_barang" placeholder="Masukkan Nama Barang" required>
                        </div>

                        <div class="form-group">
                            <label for="harga">Harga Barang</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="harga">Rp</span>
                                </div>
                                <input type="number" id="harga" class="form-control form-control-user" name="harga" placeholder="Masukkan Harga Barang" aria-describedby="harga" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="stock">Stock Barang</label>
                            <input type="number" id="stock" class="form-control form-control-user" name="stock" placeholder="Masukkan Stock Barang" required>
                        </div>

                        <div class="form-group">
                            <label for="status">Status Barang?</label>
                            <select class="form-control" id="status" name="status">
                                <option value="Tersedia">Tersedia</option>
                                <option value="Pre-Order">Pre-Order</option>
                            </select>
                        </div>

                        <div class="alert alert-warning" role="alert">
                            <strong>Ukuran gambar harus 450x300 pixel!</strong>
                        </div>

                        <div class="form-group">
                            <label for="gambar">Gambar Barang</label>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="gambar" accept="image/jpeg, image/jpg, image/png, image/gif" name="gambar" required>
                                    <label class="custom-file-label" for="inputGroupFile01">Pilih gambar</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Barang</label>
                            <textarea id="deskripsi" class="ckeditor" name="deskripsi"></textarea>
                        </div>

                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>