<?php

    date_default_timezone_set("Asia/Jakarta");

    if (isset($_POST['selesai'])) {
        $nama_kategori = $_POST['nama_kategori'];

        $queryKategori  = "INSERT INTO kategori VALUES ('', '$nama_kategori')";
        $prosesKategori = mysqli_query($conn, $queryKategori);

        if (!empty($prosesKategori)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=kategori';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Buat Kategori</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                        </div>

                        <div class="form-group">
                            <label for="nama_kategori">Nama Kategori</label>
                            <input type="text" id="nama_kategori" class="form-control form-control-user" name="nama_kategori" placeholder="Masukkan Nama Kategori" required>
                        </div>
                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>