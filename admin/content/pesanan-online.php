<?php

    if (isset($_POST['submit'])) {

        $kode_invoice   = $_POST['kode_invoice'];
        $status         = "Pengiriman";

        $queryEDIT  = "UPDATE invoice SET status='$status' WHERE kode_invoice='$kode_invoice' ";
        $prosesEDIT = mysqli_query($conn, $queryEDIT);

        if (!empty($prosesEDIT)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=pesanan-online';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-cart-arrow-down"></i> Pesanan Online</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">INVOICE</th>
                                <th scope="col">Diskon</th>
                                <th scope="col">Total Bayar</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryInvoice  = "SELECT * FROM invoice WHERE jenis_pembelian='Online' AND status!='Sukses' ORDER BY status ASC";
                                $prosesInvoice = mysqli_query($conn, $queryInvoice);
                                while ($resultInvoice   = mysqli_fetch_assoc($prosesInvoice)) {

                            ?>

                            <tr>
                                <th scope="row"><?= $no++; ?></th>
                                <td><h5 class="text-primary border border-primary font-weight-bold p-1"><?= $resultInvoice['kode_invoice']; ?></h5></td>
                                <td><?= $resultInvoice['diskon']; ?></td>
                                <td><strong>Rp<?= rp($resultInvoice['total_bayar']); ?></strong></td>
                                <td>
                                    <?php if ($resultInvoice['status']=="Pending"): ?>
                                        <button type="button" class="btn btn-warning"><i class="far fa-clock"></i> Pending</button>
                                    <?php elseif ($resultInvoice['status']=="Pengiriman"): ?>
                                        <button type="button" class="btn btn-info"><i class="fa fa-truck" aria-hidden="true"></i> Pengiriman</button>
                                    <?php elseif ($resultInvoice['status']=="Sukses"): ?>
                                        <button type="button" class="btn btn-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Selesai</button>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detail<?= $no; ?>">
                                        <i class="fas fa-external-link-alt"></i> Detail
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="detail<?= $no; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary" id="staticBackdropLabel">Detail Invoice: <u><?= $resultInvoice['kode_invoice']; ?></u></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <table class="table table-dark table-striped mt-4">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">No</th>
                                                                <th scope="col">Nama Barang</th>
                                                                <th scope="col">Harga</th>
                                                                <th scope="col">Qty</th>
                                                                <th scope="col">Sub Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                $no=1;
                                                                $totalBayar=0;
                                                                $queryBarang    = "SELECT * FROM keranjang INNER JOIN barang ON keranjang.id_barang = barang.id_barang WHERE kode_invoice='$resultInvoice[kode_invoice]' ORDER BY id_keranjang DESC";
                                                                $prosesBarang   = mysqli_query($conn, $queryBarang);
                                                                $cekBarang      = mysqli_num_rows($prosesBarang);
                                                                while ($resultBarang   = mysqli_fetch_assoc($prosesBarang)) {
                                                                    $totalBayar+=$resultBarang['sub_jumlah'];
                                                            ?>
                                    
                                                            <tr>
                                                                <th scope="row"><?= $no++; ?></th>
                                                                <td><?= $resultBarang['nama_barang']; ?></td>
                                                                <td>Rp<?= rp($resultBarang['harga']); ?></td>
                                                                <td><?= $resultBarang['qty']; ?></td>
                                                                <td>Rp<?= rp($resultBarang['sub_jumlah']); ?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                        <?php
                                                            if ($cekBarang>0) {

                                                                $queryJenisAkun   = "SELECT jenis_akun FROM akun WHERE id_akun='$resultInvoice[id_akun]'";
                                                                $prosesJenisAkun  = mysqli_query($conn, $queryJenisAkun);
                                                                $resultJenisAkun  = mysqli_fetch_assoc($prosesJenisAkun);

                                                                if ($resultJenisAkun['jenis_akun']=="Member") {
                                                                    $hargaAwal      = $totalBayar;
                                                                    $hargaDiskon    = (($hargaAwal*10)/100);
                                                                    $hargaAkhir     = $hargaAwal-$hargaDiskon;
                                                                    $diskon         = "10%";
                                                                }else{
                                                                    $hargaAkhir     = $totalBayar;
                                                                    $diskon         = "0%";
                                                                }

                                                        ?>
                                                        <tfoot>
                                                            <tr>
                                                                <th scope="col" colspan="4"><h3>TOTAL BAYAR</h3></th>
                                                                <th scope="col" colspan="2" class="text-center">
                                                                    <?php if ($resultJenisAkun['jenis_akun']=="Member"): ?>
                                                                    <h3 class="text-decoration-line-through text-danger">Rp<?= rp($hargaAwal); ?></h3>
                                                                    <small class="text-danger">Disc. 10%</small>  
                                                                    <?php endif ?>

                                                                    <h3>Rp<?= rp($hargaAkhir); ?></h3>
                                                                </th>
                                                            </tr>
                                                        </tfoot> 
                                                        <?php
                                                            }
                                                        ?>
                                                    </table>
                                                </div>
                                                <?php if ($resultInvoice['status']=="Pending"): ?>
                                                    <form action="" method="POST" class="modal-footer">
                                                        <input type="hidden" name="kode_invoice" value="<?= $resultInvoice['kode_invoice']; ?>">
                                                        <button type="submit" name="submit" class="btn btn-lg btn-block btn-primary">KIRIM PESANAN <i class="fas fa-shipping-fast"></i></button>
                                                    </form>
                                                <?php elseif ($resultInvoice['status']=="Pengiriman"): ?>
                                                    <form action="" method="POST" class="modal-footer">
                                                    </form>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>