<?php 

    // Mmemanggil connect database
    include 'functions/conn.php';
    include '../functions.php';

    session_start();

    if (empty($_SESSION['id_admin']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        header('location:logout.php');
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $_SESSION['nama_admin']; ?> - Store Peternakan</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php require 'sidebar.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require 'top-bar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <?php
                    if ($_GET['content']=="dashboard") {
                        require 'content/dashboard.php';
                    }elseif ($_GET['content']=="kategori") {
                        require 'content/kategori.php';
                    }elseif ($_GET['content']=="buat-kategori") {
                        require 'content/buat-kategori.php';
                    }elseif ($_GET['content']=="edit-kategori") {
                        require 'content/edit-kategori.php';
                    }elseif ($_GET['content']=="barang") {
                        require 'content/barang.php';
                    }elseif ($_GET['content']=="buat-barang") {
                        require 'content/buat-barang.php';
                    }elseif ($_GET['content']=="edit-barang") {
                        require 'content/edit-barang.php';
                    }elseif ($_GET['content']=="rekap-penjualan") {
                        require 'content/rekap-penjualan.php';
                    }elseif ($_GET['content']=="pesanan-online") {
                        require 'content/pesanan-online.php';
                    }elseif ($_GET['content']=="pesanan-offline") {
                        require 'content/pesanan-offline.php';
                    }else{
                        header('location: index.php?content=dashboard');
                    }
                ?>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>&copy;Dibuat oleh <?= $_SESSION['nama_admin']; ?> | Store Peternakan</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="ckeditor/ckeditor.js"></script>
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>